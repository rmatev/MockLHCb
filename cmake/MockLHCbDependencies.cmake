###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
include(${CMAKE_CURRENT_LIST_DIR}/LHCbConfigUtils.cmake)

if(NOT COMMAND lhcb_find_package)
    # Look for LHCb find_package wrapper
    find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
    if(LHCbFindPackage_FILE)
        include(${LHCbFindPackage_FILE})
    else()
        # if not found, use the standard find_package
        macro(lhcb_find_package)
            find_package(${ARGV})
        endmacro()
    endif()
endif()

# -- Public dependencies
lhcb_find_package(Gaudi REQUIRED)
# GaudiConfig.cmake does not set GAUDI_PROJECT_ROOT
if(NOT DEFINED GAUDI_PROJECT_ROOT AND Gaudi_DIR MATCHES "^(.*)/InstallArea/")
    set(GAUDI_PROJECT_ROOT "${CMAKE_MATCH_1}")
endif()

if(NOT COMMAND _gaudi_runtime_prepend)
    # _gaudi_runtime_prepend is not available in GaudiToolbox.cmake (introduced in Gaudi 36.1)
    macro(_gaudi_runtime_prepend runtime value)
        get_property(_orig_value TARGET target_runtime_paths PROPERTY runtime_${runtime})
        set_property(TARGET target_runtime_paths PROPERTY runtime_${runtime} ${value} ${_orig_value})
    endmacro()
endif()

# workaround until https://gitlab.cern.ch/gaudi/Gaudi/-/merge_requests/1465 is available
find_file(extract_qmtest_metadata extract_qmtest_metadata.py
          PATHS ${LHCb_SOURCE_DIR}/cmake # When building LHCb
                ${LHCb_DIR}              # When using an installed LHCb
          NO_DEFAULT_PATH)

# Disable hash randomization so that we have reproducible (test) runs.
# Ideally we shouldn't depend on hash() but that's not easy, see !3872 .
# See https://docs.python.org/3/using/cmdline.html#envvar-PYTHONHASHSEED
lhcb_env(SET PYTHONHASHSEED 0)

find_package(AIDA REQUIRED)
find_package(Boost 1.62 REQUIRED
    container
    date_time
    filesystem
    headers
    iostreams
    program_options
    regex
    serialization
    thread
)
find_package(CLHEP REQUIRED)
find_package(cppgsl REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(fmt REQUIRED)
find_package(GSL REQUIRED)
find_package(nlohmann_json REQUIRED)
find_package(OpenSSL REQUIRED Crypto)  # for DetDescSvc
find_package(Python REQUIRED Interpreter Development)
find_package(Rangev3 REQUIRED)
find_package(ROOT 6.20 REQUIRED
    Core
    GenVector
    Hist
    MathCore
    RIO
)
find_package(TBB REQUIRED)
find_package(Vc 1.4.1 REQUIRED)
find_package(VDT REQUIRED)
find_package(XercesC REQUIRED)
find_package(yaml-cpp REQUIRED)

find_package(PkgConfig)
pkg_check_modules(git2 libgit2 REQUIRED IMPORTED_TARGET)  # for GitEntityResolver
pkg_check_modules(zmq libzmq REQUIRED IMPORTED_TARGET)  # for ZeroMQ
pkg_check_modules(sodium libsodium REQUIRED IMPORTED_TARGET)  # for ZeroMQ

find_data_package(FieldMap REQUIRED)
find_data_package(ParamFiles REQUIRED)
find_data_package(PRConfig REQUIRED)
find_data_package(RawEventFormat REQUIRED)
find_data_package(TCK/HltTCK REQUIRED)

# -- Private dependencies
if(WITH_LHCb_PRIVATE_DEPENDENCIES)
    if(BUILD_TESTING)
        find_package(Boost 1.62 REQUIRED unit_test_framework)
        find_package(ROOT 6.20 REQUIRED Tree)
    endif()
endif()

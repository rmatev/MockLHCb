###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.packing import reco_locations, unpackers_map
from PyConf.components import force_location
from PyConf.dataflow import DataHandle
from PyConf.tonic import configurable
from PyConf.Algorithms import (UnpackMCParticle, UnpackMCVertex,
                               HltSelReportsDecoder, RawBankSizeFilter)
from PyConf.application import (default_raw_banks, make_odin,
                                make_data_with_FetchDataFromFile)
from PyConf.packing import persistreco_version, default_persistable_location as persistable_location
from GaudiConf.LbExec import InputProcessTypes, HltSourceID


@configurable
def tes_root(*, input_process: InputProcessTypes):
    """
  Get the ROOT_TES location from the input_process type
  """
    if input_process == InputProcessTypes.Spruce:
        return '/Event/Spruce/HLT2'
    elif input_process in [
            InputProcessTypes.Hlt2, InputProcessTypes.TurboPass
    ]:
        return '/Event/HLT2'
    else:
        raise NotImplementedError(
            f"The specified 'input_process' {input_process} is not recognised. Can only be 'Hlt2' or 'Spruce' or 'TurboPass'. Please check!"
        )


@configurable
def dstdata_filter(source: HltSourceID, raw_banks=default_raw_banks):
    """
    Setting a `RawBankSizeFilter` filter on the DstData bank size.

    This filter will skip events with empty DstData but a positive line decision.
    This may occur in events where DstData is a-priori huge (>16MB), and cannot be saved.

    Returns:
    The `RawBankSizeFilter` algorithm instance.
    """
    # TODO: should be able to specify the SourceID of the DstData raw bank...
    # and thus require a HltSourceID as arg to this function -- augment the Filter to check the
    #  sourceID...
    return RawBankSizeFilter(
        name='FilterDstDataSize_{}'.format(source),
        RawBankLocation=default_raw_banks("DstData"))


@configurable
def upfront_decoder(*, source: HltSourceID, raw_banks=default_raw_banks):
    """Return a DataHandle for  HltPackedBufferDecoder output
       This is input to all unpacking algorithms except MC and old unpacker
    """
    from PyConf.Algorithms import HltPackedBufferDecoder
    source = HltSourceID(source)
    return HltPackedBufferDecoder(
        name="PackedBufferDecoder_{}".format(source.name),
        SourceID=source.name,
        DecReports=raw_banks("HltDecReports"),
        RawBanks=raw_banks("DstData")).OutputBuffers


def _get_unpacked(type_name, location, **kwargs):
    mapped_dst_banks = upfront_decoder()
    if 'name' not in kwargs:
        sourceID = mapped_dst_banks.producer.name.split('_')[
            -1]  # see above `upfront_decoder()`
        kwargs['name'] = 'Unpack_{}_{} '.format(sourceID,
                                                location.replace('/', '_'))
    return unpackers_map()[type_name](
        InputName=mapped_dst_banks,
        outputs={
            "OutputName": force_location(location)
        },
        **kwargs).OutputName


def get_particles(location):
    """
    Unpacking of particles. This will also unpack the decay vertices associated with these particles implicitely
    """
    return _get_unpacked("Particles", location)


def get_pp2mcp_relations(location):
    """
    Unpacking of ProtoParticle to MCParticle relations. These are needed for truth matching
    MC Particles need to be unpacked first, proto particles are unpacked within relation unpacking
    Assume relations and MC particles are under same TES root
    """

    mc_location = location.replace(
        "Relations/ChargedPP2MCP" if "Charged" in location else
        "Relations/NeutralPP2MCP", "MC/Particles")
    mc_parts = get_mc_particles(mc_location)

    return _get_unpacked("PP2MCPRelations", location, ExtraInputs=[mc_parts])


def get_p2v_relations(location):
    """
    Unpacking of P2V relations. Make sure particles are unpacked
    """
    return _get_unpacked("P2VPRelations", location)


def get_mc_particles(location, mc_vertices: DataHandle = None):
    """
    Unpacking of MCParticles.
    Chainning MCVertices as dependency is needed for truth matching, if mc_vertices is None, it will first unpack MCVertices

    Args:
        location (str): Location of the LHCb::MCParticles
        mc_vertices (DataHandle, optional): unpacked `LHCb::MCVertices`, defaults to None.

    Returns:
        DataHandle: DataHandle of `LHCb::MCParticles`.
    """

    sim_location = location.replace("MC/Particles", "pSim/MCParticles")

    if mc_vertices is None:
        mc_vertices = get_mc_vertices(
            location.replace("MC/Particles", "MC/Vertices"))

    return UnpackMCParticle(
        name='Unpack_{}'.format(location.replace('/', '_')),
        InputName=make_data_with_FetchDataFromFile(sim_location),
        outputs={
            "OutputName": force_location(location)
        },
        ExtraInputs=[mc_vertices],
    ).OutputName


def get_mc_vertices(location):
    """
    Unpacking of MCVertices.

    Args:
        location (str): Location of the LHCb::MCParticles

    Returns:
        mc_vertices (DataHandle): DataHandle of `LHCb::MCVertices`.
    """
    sim_location = location.replace("MC/Vertices", "pSim/MCVertices")

    return UnpackMCVertex(
        name='Unpack_{}'.format(location.replace('/', '_')),
        InputName=make_data_with_FetchDataFromFile(sim_location),
        outputs={
            "OutputName": force_location(location)
        },
    ).OutputName


@configurable
def reconstruction(*, input_process: InputProcessTypes, obj: str):
    stream = tes_root(input_process=input_process)
    recos = reco_locations(stream=stream)
    reco_locs = {k: _get_unpacked(v[1], v[0]) for k, v in recos.items()}
    postprocess_unpacked_data(reco_locs)
    return reco_locs[obj]


def get_charged_protoparticles(track_type):
    return reconstruction(obj=f"{track_type}Protos")


def get_neutral_protoparticles():
    return reconstruction(obj="NeutralProtos")


def get_pvs_v1():
    return reconstruction(obj="PVs_v1")


def get_pvs():
    return reconstruction(obj="PVs")


def get_rec_summary():
    return reconstruction(obj="RecSummary")


def get_mc_track_info():
    mc_track_info = make_data_with_FetchDataFromFile('/Event/MC/TrackInfo')
    mc_track_info.force_type('LHCb::MCProperty')
    return mc_track_info


def get_mc_header(*, location='/Event/MC/Header', extra_inputs: list = []):
    """Fetch LHCb::MCHeader object from file, this object contains:
        * Pointer to primary vertices
        * Event time
        * Event number
    .. note: Accessing the primary vertices information from pointer requires the associated LHCb::MCVertex is properly unpacked, if
    extra_inputs is [], this function defaults to unpack '/Event/MC/Vertices'.

    Args:
        location (str, optional): Location of the LHCb::MCHeader, defaults to '/Event/MC/Header'.
        extra_inputs (list, optional): Extra dependencies, defaults to []

    Returns:
        DataHandle: DataHandle of the `LHCb::MCHeader`.
    """

    if not extra_inputs:
        mc_particles = get_mc_particles('/Event/MC/Particles')
        extra_inputs = [mc_particles]

    mc_header = make_data_with_FetchDataFromFile(
        location,
        ExtraInputs=extra_inputs,
        name='FetchDataFromFile_GetMCHeader')
    mc_header.force_type('LHCb::MCHeader')
    return mc_header


def get_generator_header(*, location='/Event/Gen/Header'):
    """Fetch LHCb::GenHeader object from file

    Args:
        location (str, optional): Location of the `LHCb::GenHeader`, defaults to '/Event/Gen/Header'.

    Returns:
        DataHandle: DataHandle of the `LHCb::GenHeader`.
    """
    gen_header = make_data_with_FetchDataFromFile(location)
    gen_header.force_type('LHCb::GenHeader')
    return gen_header


def get_odin():
    """
    Function to get the LHCb::ODIN location

    Args:
        options (DaVinci.Options): lbexec provided options object
    Returns:
        odin_loc: Location of the LHCb::ODIN
    """
    return make_odin()


@configurable
def get_hlt_reports(*, source: HltSourceID, raw_banks=default_raw_banks):
    """
    Set the Hlt service and algorithms.

    Args:
      source (HltSourceID): source ID or selection stage. It can be "Hlt1" or "Hlt2" or "Spruce".

    Returns:
      HltDecReportsDecoder containing the configuration for Hlt1, Hlt2 and Spruce lines.
    """

    source = HltSourceID(source)
    output_loc = "/Event/%s/DecReports" % source.name

    from PyConf.Algorithms import HltDecReportsDecoder
    return HltDecReportsDecoder(
        name=source.name + "DecReportsDecoder_{hash}",
        SourceID=source.name,
        RawBanks=raw_banks("HltDecReports"),
        outputs={'OutputHltDecReportsLocation': force_location(output_loc)},
    )


def get_decreports(source: HltSourceID):
    """
    Function to get the LHCb::DecReports for HLT1, HLT2 or Sprucing.

    Args:
        source (str): Selection stage can be "Hlt1" or "Hlt2" or "Spruce"
    Returns:
        dec_loc: Location of the LHCb::DecReports for HLT1 or Hlt2 or Spruce
    """
    return get_hlt_reports(source).OutputHltDecReportsLocation


@configurable
def get_hlt1_selreports(raw_banks=default_raw_banks):
    return HltSelReportsDecoder(
        DecReports=raw_banks("HltDecReports"),
        RawBanks=raw_banks("HltSelReports"),
        SourceID=HltSourceID.Hlt1.name).OutputHltSelReportsLocation


def postprocess_unpacked_data(data,
                              persistreco_version=persistreco_version,
                              packable=False):
    """ Needed to unpack older persistency versions into latest persistency style:
         - for PVs only in v1 format
         - for merged 'Tracks' and 'ChargedProtos' containers (to split them per track type)
         - for non-existant RecSummary

    """
    from PyConf.Algorithms import RecV1ToPVConverter
    # PV v1 only persisted for now
    data["PVs_v1"] = data["PVs"]
    data["PVs"] = RecV1ToPVConverter(
        InputVertices=data["PVs_v1"]).OutputVertices

    # per track type splitter (to convert 0.0 persistency to latest)
    if persistreco_version() == 0.0:
        if not packable:
            # use Selections (SharedObjectsContainers, so pointer remain valid to underlying objects, but not packable YET)
            # effectively front end only changed in terms of persistency locations
            from PyConf.Algorithms import TracksSharedSplitterPerType, ProtosSharedSplitterPerTrackType
            tracks_splitter = TracksSharedSplitterPerType(
                name='TrackContainerSharedSplitterPerType_{hash}',
                InputTracks=data['Tracks'])
            data['LongTracks'] = tracks_splitter.LongTracks
            data['DownstreamTracks'] = tracks_splitter.DownstreamTracks
            data['UpstreamTracks'] = tracks_splitter.UpstreamTracks
            data['Ttracks'] = tracks_splitter.Ttracks
            data['VeloTracks'] = tracks_splitter.VeloTracks
            # for protoparticles
            protos_splitter = ProtosSharedSplitterPerTrackType(
                name='ChargedProtoParticleSharedSplitterPerType_{hash}',
                InputProtos=data['ChargedProtos'])
            data['LongProtos'] = protos_splitter.LongProtos
            data['DownstreamProtos'] = protos_splitter.LongProtos
            data['UpstreamProtos'] = protos_splitter.LongProtos
        else:
            # use copies (as SharedObjectsContainer is not packable yet)
            # this can lead to weird/unwanted duplication if one will persist these objects subsequently (e.g. in a Sprucing step)
            print(
                "NB: objects will be copied to latest 'persistreco_version'"\
                ", this might lead to unwanted duplication in a subsequent packing step"
            )
            # FIXME when this is possible, move only to the SharedObjectsContainers and set these to their persistable locations
            from PyConf.Algorithms import ChargedProtoParticleFilteredCopyAlg, TrackContainerFilteredCopy
            import Functors as F
            split_containers = {}
            track_predicates = {
                'Long': F.TRACKISLONG,
                'Downstream': F.TRACKISDOWNSTREAM,
                'Upstream': F.TRACKISUPSTREAM
            }
            for track_type, track_pred in track_predicates.items():
                split_containers[
                    track_type] = ChargedProtoParticleFilteredCopyAlg(
                        name=f'ChargedProtoParticleFilteredCopyAlg_{track_type}'
                        + '_{hash}',
                        InputProtos=data['ChargedProtos'],
                        TrackPredicate=track_pred,
                        outputs={
                            'OutputProtos':
                            persistable_location(f'{track_type}Protos'),
                            'OutputTracks':
                            persistable_location(f'{track_type}Tracks'),
                            'OutputRichPIDs':
                            None,
                            'OutputMuonPIDs':
                            None,
                            'OutputMuonTracks':
                            None,
                        })
                data[f'{track_type}Protos'] = split_containers[
                    track_type].OutputProtos
                data[f'{track_type}Tracks'] = split_containers[
                    track_type].OutputTracks
                data[f'{track_type}RichPIDs'] = split_containers[
                    track_type].OutputRichPIDs
                data[f'{track_type}MuonPIDs'] = split_containers[
                    track_type].OutputMuonPIDs
                data[f'{track_type}MuonTracks'] = split_containers[
                    track_type].OutputMuonTracks
            # separate handling of T-tracks and Velo tracks
            data['Ttracks'] = TrackContainerFilteredCopy(
                name='TrackContainerFilteredCopy_Ttracks_{hash}',
                Inputs=[data['Tracks']],
                Selection=F.TRACKISTTRACK,
                outputs={
                    'Output': persistable_location('Ttracks')
                }).Output
            data['VeloTracks'] = TrackContainerFilteredCopy(
                name='TrackContainerFilteredCopy_VeloTracks_{hash}',
                Inputs=[data['Tracks']],
                Selection=F.TRACKISVELO,
                outputs={
                    'Output': persistable_location('VeloTracks')
                }).Output

    ### Temporary: This to be compatible with data where the RecSummary does not exist.
    if "RecSummary" not in data.keys():
        from PyConf.Algorithms import FakeRecSummaryMaker
        data["RecSummary"] = FakeRecSummaryMaker(
            outputs={
                "Output": persistable_location('RecSummary')
            }).Output


@configurable
def tes_root_for_tistos(*, input_process: InputProcessTypes):
    """
    Get the ROOT_TES location from the input_process type used to get trigger particles for TISTOS.
    Only input processes Spruce and TurboPass are supported.
    Note that this might not be the correct solution see https://gitlab.cern.ch/lhcb/Moore/-/issues/663#note_7295503.
    """
    if input_process == InputProcessTypes.Spruce:
        return '/Event/Spruce/HLT2/TISTOS'
    elif input_process == InputProcessTypes.TurboPass:
        return '/Event/HLT2'
    else:
        raise ValueError(
            f"Requested ROOT_TES for TISTOS for input process {input_process}. This is not supported. Please check!"
        )

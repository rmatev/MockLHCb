###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import re
from collections import OrderedDict
from functools import cache
from enum import Enum
from types import MappingProxyType

from GaudiKernel.DataHandle import DataHandle as GaudiDataHandle
from .utilities import ConfigurationError, hash_object

__all__ = [
    'dataflow_config',
    'DataHandleMode',
    'DataHandle',
    'configurable_inputs',
    'force_location',
    'getDefaultProperties',
    'getConfigurableOutputs',
]


@cache
def getDefaultProperties(configurable_type):
    return MappingProxyType(configurable_type.getDefaultProperties())


@cache
def getConfigurableOutputs(configurable_type):
    properties = getDefaultProperties(configurable_type)
    return [k for k, v in properties.items() if is_datahandle_writer(v)]


def ensure_event_prefix(location):
    # FIXME
    # Gaudi prepends /Event to some locations on the C++ side, but not to all.
    # Until it is either added or left untouched by gaudi consistently, we
    # append it ourselves to ensure this consistence
    return location if location.startswith('/Event/') else '/Event/' + location


class dataflow_config(OrderedDict):
    def apply(self):
        from .components import setup_component, _is_configurable_tool, _is_configurable_algorithm
        configurable_algs = []
        configurable_tools = []
        for type_name, conf in self.items():
            type_, name = type_name
            configurable = setup_component(type_, instance_name=name, **conf)
            if _is_configurable_algorithm(type_):
                configurable_algs.append(configurable)
            elif _is_configurable_tool(type_):
                configurable_tools.append(configurable)
            else:
                raise TypeError(
                    "{} wants to be configured but is neither of type AlgTool nor Algorithm"
                    .format(type_))
        return configurable_algs, configurable_tools


class force_location(str):
    """An indicator that a location must be set as defined.

    Algorithm output locations are usually appended by a hash defined that
    algorithm's properties and inputs. By wrapping an output DataHandle in
    `force_location`, this appending is not done.

    You almost never want to use this. Notable exceptions include the outputs
    of legacy unpacking algorithms, which must be defined exactly else
    SmartRefs pointing to those locations will break.
    """
    pass


class DataHandleMode(Enum):
    READER = "R"
    WRITER = "W"
    UPDATER = "RW"


class DataHandle(object):
    """A representation for an input/output of an Algorithm."""

    def __init__(self, producer, key, custom_location=None):
        from .components import is_algorithm
        if not producer or not key:
            raise ConfigurationError('producer or key not set correctly')
        if not is_algorithm(producer):
            raise TypeError('producer not of type Algorithm')

        self._producer = producer  # of type algorithm
        self._key = key
        if isinstance(custom_location, force_location):
            self._force_location = True
            self._custom_location = str(custom_location)
        else:
            self._force_location = False
            self._custom_location = custom_location

        self._id = hash_object((self.producer.id, self.key))

        if not self._custom_location:
            self._location = self.producer.name + '/' + self.key
        else:
            self._location = self._custom_location
            if not self._force_location:
                self._location += '/' + str(self.producer.id)  # TODO review

    @property
    def location(self):
        return ensure_event_prefix(
            self._location)  # FIXME see ensure_event_prefix description

    def __eq__(self, other):
        return self._id == other._id

    def __hash__(self):
        return self._id

    @property
    def producer(self):
        return self._producer

    @property
    def key(self):
        return self._key

    @property
    def id(self):
        return self._id

    @property
    def force_location(self):
        return self._force_location

    @property
    def type(self):
        """Return the representation of the underlying C++ type.

        Returns:
            str: The C++ type if the our producer represents the property with
            a C++ DataHandle, otherwise "unknown_t".

        """
        prop = getDefaultProperties(self._producer.type).get(self._key)
        try:
            return prop.type()
        except AttributeError:
            # if no type is known we return the forced type if it was set
            # otherwise we return unknown_t
            try:
                return self._forced_type
            except AttributeError:
                return "unknown_t"

    def __repr__(self):
        return 'DataHandle({!r})'.format(self.location)

    def untyped(self):
        custom_location = (force_location(self._custom_location)
                           if self._force_location else self._custom_location)
        return UntypedDataHandle(self._producer, self._key, custom_location)

    def force_type(self, cpp_type: str):
        """Force the C++ type of the DataHandle.

        Note: This is usually only needed when using untyped DataHandles from
        e.g. FetchDataFromFile that are then passed to the TES functor or other
        consumers that require correct C++ type information.

        """
        if self.type == cpp_type:
            return
        if self.type != 'unknown_t':
            raise ConfigurationError(
                "Forcing the C++ type of an already typed DataHandle is not supported"
            )
        self._forced_type = cpp_type


class UntypedDataHandle(DataHandle):
    """A DataHandle with an unknown_t type."""

    @property
    def type(self):
        return "unknown_t"


def is_datahandle(arg):
    """Returns True if arg is of type DataHandle"""
    return isinstance(arg, DataHandle)


def is_datahandle_writer(x):
    """Return True if x is a writer DataHandle or a list of them.

    Returns False if x is an empty list.
    """
    if not isinstance(x, list):
        return (isinstance(x, GaudiDataHandle)
                and x.mode() != DataHandleMode.READER.value)
    # Ignore empty lists, then check that all elements are DataHandle writers
    return False if not x else all(
        isinstance(i, GaudiDataHandle)
        and i.mode() != DataHandleMode.READER.value for i in x)


def configurable_inputs(alg_type):
    """Return a dict of all input properties of alg_type."""
    return {
        key: val
        for key, val in getDefaultProperties(alg_type).items()
        if (isinstance(val, GaudiDataHandle)
            and val.mode() != DataHandleMode.WRITER.value)
    }


def convertible_types(input_type):
    """Return C++ types that can be converted to `input_type`."""

    RANGE_VECTOR_CONST_RE = re.compile(
        r'^Gaudi::(Named)?Range_<'
        r'(?P<CONTAINER>std::vector<(?P<TYPE>.*) const\*,(?P<ALLOCATOR>.*) >),'
        r'__gnu_cxx::__normal_iterator<(?P=TYPE) const\* const\*,(?P=CONTAINER) >'
        r' >$')
    KEYED_CONTAINER = ('KeyedContainer<{type},'
                       'Containers::KeyedObjectManager<Containers::hashmap> >')
    SELECTION = 'SharedObjectsContainer<{type}>'

    m = RANGE_VECTOR_CONST_RE.match(input_type)
    if m:
        t = m.group('TYPE')
        return [
            input_type,
            KEYED_CONTAINER.format(type=t),
            SELECTION.format(type=t),
        ]

    return input_type


def is_convertible_type(from_type, to_type):
    if from_type == 'unknown_t' or to_type == 'unknown_t':
        return True
    return from_type in convertible_types(to_type)

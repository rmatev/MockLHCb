###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import ApplicationMgr, LHCbApp

from Configurables import (ProduceProtoParticles, MultiPacker, GitANNSvc)

from PRConfig import TestFileDB
import json
from PyConf.components import force_location

loc = "/Event/Fake/ProtoParticles"
produceProtoParticle = ProduceProtoParticles(Output=loc)

locations = {}
locations[loc] = 0

d = {'PackedObjectLocations': {1: loc}}

hltann = GitANNSvc(Overrule={1: json.dumps(d)})

partPacker = MultiPacker(
    ProtoParticles=[force_location(loc)],
    OutputName="/Event/Fake/Packed/ProtoParticles",
    ANNSvc=hltann.getFullName(),
    EncodingKey=1,
    EnableCheck=True,
    OutputLevel=5)

ApplicationMgr().TopAlg = [produceProtoParticle, partPacker]

app = LHCbApp()
app.EvtMax = 10

f = TestFileDB.test_file_db[
    "2017HLTCommissioning_BnoC_MC2015_Bs2PhiPhi_L0Processed"]
f.setqualifiers(configurable=app)
f.run(configurable=app, withDB=True)

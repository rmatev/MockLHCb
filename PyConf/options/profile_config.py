###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options to profile the python configuration with gaudirun.py.

This gaudirun.py options file works by starting the profiling whenever
it is imported by gaudirun.py and stopping it in a post-config action
after all python options and ConfigurableUsers have finished executing.

In order to use it, add this to the beginning of your list of options:

    gaudirun.py profile-config.py options.py

A ``.pstats`` and a ``.svg`` file will be automatically produced.

To use the ``.pstats`` dump by hand, you may do, for example:

    LHCb/run gprof2dot.py --skew 0.2 -f pstats `ls -t *.pstats | head -1` \
          | LHCb/run dot -Tsvg -o profile.svg

gprof2dot.py is shipped in LHCb, but can also be downloaded with
    curl -L -O https://raw.githubusercontent.com/jrfonseca/gprof2dot/master/gprof2dot.py

"""
import cProfile
import pstats
import io
import sys
import os
import time
import subprocess
from datetime import datetime
from Gaudi.Configuration import appendPostConfigAction

profile = cProfile.Profile()

profile_name = ('profile-{:%Y%m%dT%H%M}-{}'.format(datetime.now(),
                                                   os.getpid()))


def start_profile():
    profile.enable()


def stop_profile(sortby='cumulative'):
    profile.disable()

    cpu_time = time.process_time()
    sys.stderr.write('Elapsed CPU time {:.1f}\n'.format(cpu_time))

    s = io.StringIO()
    ps = pstats.Stats(profile, stream=s).sort_stats(sortby)
    ps.dump_stats(profile_name + '.pstats')

    # filter and print only the functions above 10% of the total time
    new_list = []
    for func in ps.fcn_list:
        cumtime = ps.stats[func][3]
        if cumtime > 0.1 * ps.total_tt:
            new_list.append(func)
    ps.fcn_list = new_list
    ps.print_stats()
    sys.stderr.write(s.getvalue())

    # create an svg if gprof2dot.py and dot are available
    subprocess.run(
        f"gprof2dot.py --skew 0.2 -f pstats {profile_name}.pstats " +
        f" | dot -Tsvg -o {profile_name}.svg",
        shell=True)


# Post-config action inception, or how do we make sure this is really
# the last thing to be called
appendPostConfigAction(lambda: appendPostConfigAction(stop_profile))

start_profile()

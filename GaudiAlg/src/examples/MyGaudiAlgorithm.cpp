/***********************************************************************************\
* (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/
#include "IMyTool.h"

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ToolHandle.h"

/// Trivial Algorithm for tutorial purposes
class MyGaudiAlgorithm : public GaudiAlgorithm {
public:
  /// Constructor of this form must be provided
  MyGaudiAlgorithm( const std::string& name, ISvcLocator* pSvcLocator );

  /// Three mandatory member functions of any algorithm
  StatusCode initialize() override;
  StatusCode execute() override;
  StatusCode finalize() override;

  bool isClonable() const override { return true; }

private:
  Gaudi::Property<std::string> m_privateToolType{this, "ToolWithName", "MyTool",
                                                 "Type of the tool to use (internal name is ToolWithName)"};

  IMyTool* m_privateTool  = nullptr;
  IMyTool* m_publicTool   = nullptr;
  IMyTool* m_privateGTool = nullptr;
  IMyTool* m_publicGTool  = nullptr;

  IMyTool* m_privateToolWithName = nullptr;

  IMyOtherTool* m_privateOtherInterface = nullptr;

  ToolHandle<IMyTool> m_legacyToolHandle{"MyTool/LegacyToolHandle", this};

  ToolHandle<IMyTool>       m_myPrivToolHandle{this, "PrivToolHandle", "MyTool/PrivToolHandle"};
  PublicToolHandle<IMyTool> m_myPubToolHandle{this, "PubToolHandle", "MyTool/PubToolHandle"};

  PublicToolHandle<IAlgTool> m_myGenericToolHandle{this, "GenericToolHandle", "MyTool/GenericToolHandle"};

  ToolHandle<IAlgTool> m_myUnusedToolHandle{this, "UnusedToolHandle", "TestToolFailing/UnusedToolHandle"};

  ToolHandle<IMyTool> m_undefinedToolHandle{this};
  ToolHandle<IMyTool> m_invalidToolHandle{this, "InvalidToolHandle", "TestToolFailing"};

  ToolHandle<IWrongTool> m_wrongIfaceTool{this, "WrongIfaceTool", "MyTool/WrongIfaceTool"};

  PublicToolHandle<const IMyTool> m_myConstToolHandle{"MyTool/ConstGenericToolHandle"};

  PublicToolHandle<const IMyTool> m_myCopiedConstToolHandle;
  PublicToolHandle<const IMyTool> m_myCopiedConstToolHandle2;
  PublicToolHandle<IMyTool>       m_myCopiedToolHandle;

  PublicToolHandleArray<IMyTool> m_tha{this,
                                       "MyPublicToolHandleArrayProperty",
                                       {"MyTool/AnotherConstGenericToolHandle", "MyTool/AnotherInstanceOfMyTool"}};

  DataObjectReadHandle<DataObject> m_tracks{this, "tracks", "/Event/Rec/Tracks", "the tracks"};
  DataObjectReadHandle<DataObject> m_hits{this, "hits", "/Event/Rec/Hits", "the hits"};
  DataObjectReadHandle<DataObject> m_raw{this, "raw", "/Rec/RAW", "the raw stuff"};

  DataObjectWriteHandle<DataObject> m_selectedTracks{this, "trackSelection", "/Event/MyAnalysis/Tracks",
                                                     "the selected tracks"};
};

// Static Factory declaration
DECLARE_COMPONENT( MyGaudiAlgorithm )

MyGaudiAlgorithm::MyGaudiAlgorithm( const std::string& name, ISvcLocator* ploc ) : GaudiAlgorithm( name, ploc ) {
  // Keep at least one old-style ToolHandle property to test compilation
  declareProperty( "LegacyToolHandle", m_legacyToolHandle );
  declareProperty( "UndefinedToolHandle", m_undefinedToolHandle );

  m_myCopiedConstToolHandle  = m_myPubToolHandle;
  m_myCopiedToolHandle       = m_myPubToolHandle;
  m_myCopiedConstToolHandle2 = m_myConstToolHandle;
}

StatusCode MyGaudiAlgorithm::initialize() {
  return GaudiAlgorithm::initialize().andThen( [&] {
    info() << "initializing...." << endmsg;

    m_publicTool            = tool<IMyTool>( "MyTool" );
    m_privateTool           = tool<IMyTool>( "MyTool", this );
    m_publicGTool           = tool<IMyTool>( "MyGaudiTool" );
    m_privateGTool          = tool<IMyTool>( "MyGaudiTool", this );
    m_privateToolWithName   = tool<IMyTool>( m_privateToolType, "ToolWithName", this );
    m_privateOtherInterface = tool<IMyOtherTool>( "MyGaudiTool", this );

    // disable ToolHandle
    m_myUnusedToolHandle.disable();

    info() << m_tracks.objKey() << endmsg;
    info() << m_hits.objKey() << endmsg;
    info() << m_raw.objKey() << endmsg;

    info() << m_selectedTracks.objKey() << endmsg;

    // m_wrongIfaceTool is being retrieved via the wrong interface.
    // we expect the retrieve() to throw an exception.
    try {
      if ( m_wrongIfaceTool.retrieve().isFailure() ) {
        error() << "unable to retrieve " << m_wrongIfaceTool.typeAndName() << " (unexpected)" << endmsg;
        m_wrongIfaceTool.disable();
      }
    } catch ( GaudiException& ex ) {
      info() << "unable to retrieve " << m_wrongIfaceTool.typeAndName() << " (expected) with exception: " << ex.what()
             << endmsg;
      m_wrongIfaceTool.disable();
    }

    info() << "....initialization done" << endmsg;
    return StatusCode::SUCCESS;
  } );
}

StatusCode MyGaudiAlgorithm::execute() {
  info() << "executing...." << endmsg;

  info() << "tools created with tool<T>..." << endmsg;

  m_publicTool->doIt();
  m_privateTool->doIt();
  m_publicGTool->doIt();
  m_privateGTool->doIt();
  m_privateToolWithName->doIt();
  m_privateOtherInterface->doItAgain();

  info() << "tools created via ToolHandle<T>...." << endmsg;

  m_myPrivToolHandle->doIt();
  m_myPubToolHandle->doIt();
  m_myConstToolHandle->doIt();

  info() << "tools copied assigned via ToolHandle<T>...." << endmsg;

  m_myCopiedConstToolHandle->doIt();
  m_myCopiedToolHandle->doIt();
  m_myCopiedConstToolHandle2->doIt();

  info() << "tools copied constructed via ToolHandle<T>...." << endmsg;

  // copy construct some handles
  ToolHandle<const IMyTool> h1( m_myPubToolHandle );
  ToolHandle<IMyTool>       h2( m_myPrivToolHandle );
  ToolHandle<const IMyTool> h3( m_myConstToolHandle );
  h1->doIt();
  h2->doIt();
  h3->doIt();

  return StatusCode::SUCCESS;
}

StatusCode MyGaudiAlgorithm::finalize() {
  info() << "finalizing...." << endmsg;
  return GaudiAlgorithm::finalize();
}

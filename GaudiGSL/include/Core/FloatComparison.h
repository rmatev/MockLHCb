/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <algorithm>
#include <limits>

namespace LHCb {

  /**
   * Generic method to compare two floating point values
   * ADL is used to use standard library by default but switch to custom
   * methods for abs and max in case of custom types, e.g. vector types of SIMDWrapper
   * Note that is should not be used to compare to 0, for this essentiallyZero
   * should be used
   */
  template <typename T>
  constexpr bool essentiallyEqual( T const a, T const b ) {
    using std::abs, std::max;
    return abs( a - b ) <= max( abs( a ), abs( b ) ) * std::numeric_limits<T>::epsilon();
  }

  /**
   * Generic method to compare a floating point value to 0
   * ADL is used to use standard library by default but switch to custom
   * methods for abs in case of custom types, e.g. vector types of SIMDWrapper
   */
  template <typename T>
  constexpr bool essentiallyZero( T const a ) {
    using std::abs;
    return abs( a ) <= std::numeric_limits<T>::min();
  }

} // namespace LHCb
